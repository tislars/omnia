<?php
namespace Task;

use Mage\Task\AbstractTask;

class Permissions extends AbstractTask
{
    public function getName()
    {
        return 'Fixing file permissions';
    }

    public function run()
    {
        $command = 'chmod +x bin/console';
        $result = $this->runCommandRemote($command);

//        $command = 'HTTPDUSER=`ps axo user,comm | grep -E \'[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx\' | grep -v root | head -1 | cut -d\  -f1`';
//        $result = $this->runCommandRemote($command);
//
//        $command = 'sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var';
//        $result = $this->runCommandRemote($command);
//
//        $command = 'sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var';
//        $result = $this->runCommandRemote($command);

        return $result;
    }
}